﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace InternDBAPI.Models
{
    public class Department
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; } = string.Empty;

        [BsonElement("name")]
        public string? Name { get; set; } = string.Empty;

        [BsonElement("Concentrations")]
        public List<Concentration>? Concentrations { get; set; }
    }
}
