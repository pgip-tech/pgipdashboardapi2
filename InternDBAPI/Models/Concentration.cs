﻿using MongoDB.Bson.Serialization.Attributes;

namespace InternDBAPI.Models
{
    public class Concentration
    {
        [BsonElement("name")]
        public string Name { get; set; } = string.Empty;

        [BsonElement("description")]
        public string Description { get; set; } = string.Empty;
    }
}
