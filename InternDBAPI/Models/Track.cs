﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace InternDBAPI.Models
{
    public class Track
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; } = string.Empty;

        [BsonElement("trackName")]
        public required string TrackName { get; set; }

        [BsonElement("startDate")]
        public required long StartDate { get; set; } = DateTime.Now.Millisecond;

        [BsonElement("endDate")]
        public required long EndDate { get; set; } = DateTime.MaxValue.Millisecond;
    }
}
