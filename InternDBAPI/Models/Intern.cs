﻿using InternDBAPI.Enums;
using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace InternDBAPI.Models
{
    public class Intern
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public required string Id { get; set; } = string.Empty;
        
        [BsonElement("firstname")]
        public required string FirstName { get; set; }
        
        [BsonElement("lastname")]
        public required string LastName { get; set; }

        [BsonElement("pgipEmail")]
        public string? PGIPEmail { get; set; }  
        
        [BsonElement("purdueEmail")]
        public required string PurdueEmail { get; set; }
        
        [BsonElement("personalEmail")]
        public required string PersonalEmail { get; set; }
        
        [BsonElement("department")]
        public string? Department { get; set; }
        
        [BsonElement("degree")]
        public string? Degree { get; set; }
        
        [BsonElement("concentration")]
        public string? Concentration { get; set; }
        
        [BsonElement("role")]
        public string? Role { get; set; }
        
        [BsonElement("track")]
        public string? Track { get; set; }

        [BsonElement("startDate")]
        public required long StartDate { get; set; } = DateTime.Now.Millisecond;
        
        [BsonElement("endDate")]
        public long EndDate { get; set; }

        [BsonElement("status")]
        public string Status { get; set; } = "Intern";
        
        [BsonElement("isInvitedToMeetings")]
        public bool isInvitedToMeetings { get; set; }
        
        [BsonElement("isEmailSent")]
        public bool isEmailSent { get; set; }
        
        [BsonElement("isTCWAccessGiven")]
        public bool isTCWAccessGiven { get; set; }
        
        [BsonElement("isOneDriveSetupCompleted")]
        public bool isOneDriveSetupCompleted { get; set; }
        
        [BsonElement("isMasterClassCompleted")]
        public bool isMasterClassCompleted { get; set; }
        
        [BsonElement("isExitSurveySent")]
        public bool isExitSurveySent { get; set; }
        
        [BsonElement("isEntranceSurveySent")]
        public bool isEntranceSurveySent { get; set; }
        
        [BsonElement("isWelcomeDocumentSent")]
        public bool isWelcomeDocumentSent { get; set; }
        
        [BsonElement("isPlannerAccessGiven")]
        public bool isPlannerAccessGiven { get; set; }
        
        [BsonElement("isThreeSignedDocuments")]
        public bool isThreeSignedDocuments { get; set; }
        
        [BsonElement("isESETTrainingCompleted")]
        public bool isESETTrainingCompleted { get; set; }
        
        [BsonElement("isOrientation1Completed")]
        public bool isOrientation1Completed { get; set; }
        
        [BsonElement("isOrientation2Completed")]
        public bool isOrientation2Completed { get; set; }
        
        [BsonElement("isResumeSent")]
        public bool isResumeSent { get; set; }
        
        [BsonElement("isTeamGroupChat")]
        public bool isTeamGroupChat { get; set; }


    }
}
