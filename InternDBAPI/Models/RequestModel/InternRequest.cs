﻿using InternDBAPI.Enums;
using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace InternDBAPI.Models.RequestModel
{
    public class InternRequest
    {

        [BsonElement("firstname")]
        public string? FirstName { get; set; }

        [BsonElement("lastname")]
        public string? LastName { get; set; }

        [BsonElement("pgipEmail")]
        public string? PGIPEmail { get; set; }

        [BsonElement("purdueEmail")]
        public string? PurdueEmail { get; set; }

        [BsonElement("personalEmail")]
        public string? PersonalEmail { get; set; }

        [BsonElement("department")]
        public string? Department { get; set; }

        [BsonElement("degree")]
        public string? Degree { get; set; }

        [BsonElement("concentration")]
        public string? Concentration { get; set; }

        [BsonElement("role")]
        public string? Role { get; set; }

        [BsonElement("track")]
        public string? Track { get; set; }

        [BsonElement("startDate")]
        public long StartDate { get; set; } = DateTime.Now.Millisecond;

        [BsonElement("endDate")]
        public long EndDate { get; set; }

        [BsonElement("status")]
        public string Status { get; set; } = StatusEnums.Intern.ToString();

        [BsonElement("isInvitedToMeetings")]
        public bool isInvitedToMeetings { get; set; }

        [BsonElement("isEmailSent")]
        public bool isEmailSent { get; set; }

        [BsonElement("isTCWAccessGiven")]
        public bool isTCWAccessGiven { get; set; }

        [BsonElement("isOneDriveSetupCompleted")]
        public bool isOneDriveSetupCompleted { get; set; }

        [BsonElement("isMasterClassCompleted")]
        public bool isMasterClassCompleted { get; set; }

        [BsonElement("isExitSurverySent")]
        public bool isExitSurverySent { get; set; }

        [BsonElement("isEntranceSurverySent")]
        public bool isEntranceSurveySent { get; set; }

        [BsonElement("isWelcomeDocumentSent")]
        public bool isWelcomeDocumentSent { get; set; }

        [BsonElement("isPlannerAccessGiven")]
        public bool isPlannerAccessGiven { get; set; }

        [BsonElement("isThreeSignedDocuments")]
        public bool isThreeSignedDocuments { get; set; }

        [BsonElement("isESETTrainingCompleted")]
        public bool isESETTrainingCompleted { get; set; }

        [BsonElement("isOrientation1Completed")]
        public bool isOrientation1Completed { get; set; }

        [BsonElement("isOrientation2Completed")]
        public bool isOrientation2Completed { get; set; }

        [BsonElement("isResumeSent")]
        public bool isResumeSent { get; set; }

        [BsonElement("isTeamGroupChatSent")]
        public bool isTeamGroupChat { get; set; }


    }
}
