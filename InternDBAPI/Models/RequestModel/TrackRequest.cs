﻿using MongoDB.Bson.Serialization.Attributes;
using MongoDB.Bson;

namespace InternDBAPI.Models.RequestModel
{
    public class TrackRequest
    {

        public string Id { get; set; } = string.Empty;

        public string? TrackName { get; set; }

        public long StartDate { get; set; } = DateTime.MaxValue.Millisecond;

        public long EndDate { get; set; } = DateTime.MaxValue.Millisecond;
    }
}
