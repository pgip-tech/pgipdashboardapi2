﻿namespace InternDBAPI.Models.RequestModel
{
    public class CreateDepartmentRequest
    {
        public required string Name { get; set; } = string.Empty;

        public List<Concentration>? Concentrations { get; set; }



    }
}
