﻿namespace InternDBAPI.Models.ResponseModel
{
    public class JWTValidationResponse
    {
        public string FirstName { get; set; } = string.Empty;

        public bool isExpired { get; set; } = true;

    }
}
