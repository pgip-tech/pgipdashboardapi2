﻿namespace InternDBAPI.Models.ResponseModel
{
    public class LoginResponse
    {
        public string Id { get; set; } = string.Empty;
        public string Token { get; set; } = string.Empty;
        public string Email { get; set; } = string.Empty;
    }
}
