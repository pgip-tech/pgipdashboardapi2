﻿using MongoDB.Bson;
using MongoDB.Bson.Serialization.Attributes;

namespace InternDBAPI.Models
{
    public class Admin
    {
        [BsonId]
        [BsonRepresentation(BsonType.ObjectId)]
        public string Id { get; set; } = string.Empty;

        [BsonElement("Email")]
        public required string Email { get; set; }

        [BsonElement("Password")]
        public required string Password { get; set; }

        //Levels are 3,2, 1; 3 is Highest and 1 is lowest
        [BsonElement("PermissionLevel")]
        public int PermissionLevel { get; set; } = 0;
    }
}
