﻿using InternDBAPI.Interfaces;

namespace InternDBAPI.Services
{
    public class InternDBSettings : IInternDBSettings
    {
        public required string InternsCollection { get; set; }
        public required string AdminsCollection { get; set; }
        public required string TracksCollection { get; set; }
        public required string DepartmentsCollection { get; set; }
        public required string ConnectionString { get; set; }
        public required string DatabaseName { get; set; }

    }
}
