﻿using InternDBAPI.Models;

namespace InternDBAPI.Classes
{
    public class PWDHasher : Hasher
    {
        public PWDHasher(string email, string pass) : base(email, pass)
        {
            
        }


        public Admin GetHashed()
        {
            Admin admin = new Admin()
            {
                Email = Email,
                Password = Pass
            };

            return admin;
        }
    }
}
