﻿using InternDBAPI.Models;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;

namespace InternDBAPI.Classes
{
    public class CreateJWT
    {

        private readonly IConfiguration _config;


        public CreateJWT(IConfiguration config)
        {
            _config = config;
        }

        public string GetJWT(Admin admin)
        {
            return GenerateJSONWebToken(admin);
        }

        private string GenerateJSONWebToken(Admin admin)
        {
            List<Claim> claims = new List<Claim> {
                new Claim(ClaimTypes.Email, admin.Email),
                new Claim(ClaimTypes.Role, admin.PermissionLevel.ToString()),
            };

            var key = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(
                    _config.GetSection("AppSettings:Token").Value!));


            var creds = new SigningCredentials(key, SecurityAlgorithms.HmacSha512Signature);

            var token = new JwtSecurityToken(
                    claims: claims,
                    expires: DateTime.Now.AddDays(1),
                    signingCredentials: creds
                );

            var jwt = new JwtSecurityTokenHandler().WriteToken(token);

            return jwt;
        }
    }
}
