﻿using Microsoft.AspNetCore.Identity;
using System.Diagnostics.Tracing;

namespace InternDBAPI.Classes
{
    public abstract class Hasher
    {
        public string Email { get; set; } = string.Empty;

        public string Pass { get; set; } = string.Empty;

        public Hasher(string email, string password)
        {
            SetHashedPass(email, password);
        }

        private void SetHashedPass(string email, string password)
        {
            PasswordHasher<dynamic> passwordHasher = new PasswordHasher<dynamic>();
            var hashedPass = passwordHasher.HashPassword(email, password);

            Email = email;
            Pass = hashedPass;
        }

        

    }
}
