﻿using InternDBAPI.Models;

namespace InternDBAPI.Interfaces
{
    public interface IAdminRepo
    {
        Admin Create(Admin admin);
        void Delete(string id);
        List<Admin> GetAll();
        Admin GetByEmail(string email);
        Admin GetById(string id);
    }
}