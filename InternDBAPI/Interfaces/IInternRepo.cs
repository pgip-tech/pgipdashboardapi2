﻿using InternDBAPI.Models;

namespace InternDBAPI.Interfaces
{
    public interface IInternRepo
    {
        Intern CreateIntern(Intern intern);
        List<Intern> GetAll();
        List<Intern> GetByDepartment(string department);
        Intern GetById(string id);
        Intern GetByPgipEmail(string email);
        void Update(string id, Intern intern);
    }
}