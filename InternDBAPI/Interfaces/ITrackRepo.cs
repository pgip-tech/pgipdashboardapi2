﻿using InternDBAPI.Models;

namespace InternDBAPI.Interfaces
{
    public interface ITrackRepo
    {
        void CreateTrack(Track track);
        void DeleteTrack(string id);
        List<Track> GetAllTracks();
        Track GetTrackById(string id);
        Track GetTrackByName(string trackName);
        List<Track> UpdateAllTracks(List<Track> tracks);
        void UpdateTrack(string id, Track track);
    }
}