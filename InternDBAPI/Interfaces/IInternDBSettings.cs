﻿namespace InternDBAPI.Interfaces
{
    public interface IInternDBSettings
    {
        string ConnectionString { get; set; }
        string DatabaseName { get; set; }
        string InternsCollection { get; set; }
        string AdminsCollection { get; set; }
        string TracksCollection { get; set; }

        string DepartmentsCollection { get; set; }
    }
}