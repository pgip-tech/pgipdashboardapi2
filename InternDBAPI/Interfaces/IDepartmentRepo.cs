﻿using InternDBAPI.Models;

namespace InternDBAPI.Interfaces
{
    public interface IDepartmentRepo
    {
        void Create(Department department);
        void Delete(string id);
        void Edit(Department newDepartment);
        List<Department> GetAll();
        Department GetById(string id);
        Department GetByName(string name);
    }
}