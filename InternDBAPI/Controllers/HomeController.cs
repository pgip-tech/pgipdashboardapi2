﻿using InternDBAPI.Classes;
using InternDBAPI.Interfaces;
using InternDBAPI.Models;
using InternDBAPI.Models.RequestModel;
using InternDBAPI.Models.ResponseModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;
using System.Security.Claims;
using System.Xml;

namespace InternDBAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]

    public class HomeController : Controller
    {
        private readonly IInternRepo _internRepo;
        private readonly IConfiguration _config;
        private readonly IAdminRepo _adminRepo;

        public HomeController(IInternRepo internRepo, IConfiguration config, IAdminRepo adminRepo)
        {
            _internRepo = internRepo;
            _config = config;
            _adminRepo = adminRepo;
        }


        [HttpPost("Login")]
        public IActionResult Login(LoginRequest loginRequest)
        {
            if (String.IsNullOrWhiteSpace(loginRequest.Email) || String.IsNullOrWhiteSpace(loginRequest.Password))
            {
                return BadRequest("Invalid Email or Password");
            }

            var foundAdmin = _adminRepo.GetByEmail(loginRequest.Email);

            if (foundAdmin is null) return BadRequest("Invalid Email or password");

            try
            {
                PasswordHasher<Admin> passwordHasher = new PasswordHasher<Admin>();

                //This is just here so that I can uncomment and generate a hashed pass for the real admins prior to true deployment for now
                //May make a hidden view for this assuming that checks DB to see if there is no admins in it?
                //PWDHasher wDHasher = new PWDHasher(loginRequest.Email, loginRequest.Password);
                //var pass = wDHasher.GetHashed();
                //Console.WriteLine("test");
                var result = passwordHasher.VerifyHashedPassword(foundAdmin, foundAdmin.Password, loginRequest.Password);

                if (result is not PasswordVerificationResult.Success)
                {
                    return BadRequest("Invalid Email or Password");
                }
            }
            catch
            {
                return BadRequest("Invalid Email or Password");
            }

            LoginResponse response = new LoginResponse()
            {
                Id = foundAdmin.Id,
                Token = GenerateToken(foundAdmin),
                Email = loginRequest.Email
            };

            return Ok(response);
        }

        [HttpGet("Validate")]
        [Authorize]
        public IActionResult ValidateUser() 
        {
            
            var userEmail = User.FindFirst(ClaimTypes.Email)?.Value;
            var expirationClaim = User.FindFirst("exp")?.Value;

            if (userEmail is null || expirationClaim is null)
            {
                return BadRequest("Not Validated");
            }

            var ticks = long.Parse(expirationClaim);
            var tokenDate = DateTimeOffset.FromUnixTimeSeconds(ticks).UtcDateTime;
            var now = DateTime.Now.ToUniversalTime();

            if (tokenDate <= now)
            {
                return BadRequest("Expired");
            }

            string[] splitted = userEmail.Split('.');

            
            JWTValidationResponse response = new JWTValidationResponse() 
            { 
                FirstName = splitted[0],
                isExpired = false,
            };

            return Ok(response);
        }




        [HttpPost("ResetPassword")]
        public IActionResult ResetPassword()
        {
            return Ok();
        }


        private string GenerateToken(Admin admin)
        {
            CreateJWT createJWT = new CreateJWT(_config);
            return createJWT.GetJWT(admin);
        }
    }
}
