﻿using InternDBAPI.Interfaces;
using InternDBAPI.Models;
using InternDBAPI.Models.RequestModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using ZstdSharp.Unsafe;

namespace InternDBAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]

    public class InternsController : Controller
    {
        private readonly IInternRepo _internRepo;

        public InternsController(IInternRepo internRepo)
        {
            _internRepo = internRepo;
        }

        [HttpGet("GetAll")]
        public IActionResult GetAllInterns()
        {
            var interns = _internRepo.GetAll();
            return Ok(interns);
        }

        [HttpGet("GetById")]
        public IActionResult GetInternById(string id) 
        {
            if (id is null) return BadRequest("Invalid Id");
            
            var intern = _internRepo.GetById(id);
            if (intern is null) return NotFound($"User with id of '{id}' was not found");

            return Ok(intern);
        }

        [HttpPost("Create")]
        [Authorize(Roles = "1")]
        public IActionResult CreateIntern([FromBody] InternRequest internRequest)
        {
            if (internRequest == null) return BadRequest("Invalid Request.");

            try
            {
                var intern = new Intern()
                {
                    Id = string.Empty,
                    FirstName = internRequest.FirstName.ToLower(),
                    LastName = internRequest.LastName.ToLower(),
                    PGIPEmail = internRequest.PGIPEmail.ToLower(),
                    PurdueEmail = internRequest.PurdueEmail.ToLower(),
                    PersonalEmail = internRequest.PersonalEmail.ToLower(),
                    Department = internRequest.Department,
                    Degree = internRequest.Degree,
                    Concentration = internRequest.Concentration,
                    Role = internRequest.Role,
                    Track = internRequest.Track,
                    StartDate = internRequest.StartDate,
                    EndDate = internRequest.EndDate,
                    Status = internRequest.Status,
                    isInvitedToMeetings = internRequest.isInvitedToMeetings,
                    isEmailSent = internRequest.isEmailSent,
                    isTCWAccessGiven = internRequest.isTCWAccessGiven,
                    isOneDriveSetupCompleted = internRequest.isOneDriveSetupCompleted,
                    isMasterClassCompleted = internRequest.isMasterClassCompleted,
                    isExitSurveySent = internRequest.isExitSurverySent,
                    isEntranceSurveySent = internRequest.isEntranceSurveySent,
                    isWelcomeDocumentSent = internRequest.isWelcomeDocumentSent,
                    isPlannerAccessGiven = internRequest.isPlannerAccessGiven,
                    isThreeSignedDocuments = internRequest.isThreeSignedDocuments,
                    isESETTrainingCompleted = internRequest.isESETTrainingCompleted,
                    isOrientation1Completed = internRequest.isOrientation1Completed,
                    isOrientation2Completed = internRequest.isOrientation2Completed,
                    isResumeSent = internRequest.isResumeSent,
                    isTeamGroupChat = internRequest.isTeamGroupChat
                };

                return Ok(_internRepo.CreateIntern(intern));
            }
            catch (Exception)
            {
                return BadRequest("Something went wrong. Please try again later");
            }
        }

        [HttpPut]
        [Authorize(Roles = "1")]
        public IActionResult EditIntern(Intern intern)
        {
            if (intern is null) return BadRequest("Please select a valid intern");

            try
            {
                _internRepo.Update(intern.Id, intern);
                return Ok("Intern Updated");
            }
            catch (Exception)
            {
                return BadRequest("Something went wrong. Please try again later");
            }

        }
    }
}
