﻿using InternDBAPI.Interfaces;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace InternDBAPI.Controllers
{

    [ApiController]
    [Route("[controller]")]
    [Authorize]

    public class AdminsController : Controller
    {
        private readonly IAdminRepo _adminRepo;

        public AdminsController(IAdminRepo adminRepo)
        {
            _adminRepo = adminRepo;
        }

        [HttpPost, Authorize(Roles = "1")]
        public IActionResult CreateAdmin()
        {
            return Ok();
        }

        [HttpPut, Authorize(Roles = "1")]
        public IActionResult EditAdmin() 
        {
            return BadRequest();
        }

        [HttpDelete, Authorize(Roles = "1")]
        public IActionResult DeleteAdmin() 
        { 
            return NoContent();
        }

        [HttpGet, Authorize]
        public IActionResult GetAdmins() 
        {
            return Ok(_adminRepo.GetAll());
        }
    }
}
