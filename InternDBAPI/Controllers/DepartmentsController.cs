﻿using InternDBAPI.Interfaces;
using InternDBAPI.Models;
using InternDBAPI.Models.RequestModel;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using MongoDB.Driver;

namespace InternDBAPI.Controllers
{

    [ApiController]
    [Route("[controller]")]
    [Authorize]

    public class DepartmentsController : Controller
    {
        private readonly IDepartmentRepo _departmentRepo;

        public DepartmentsController(IDepartmentRepo departmentRepo)
        {
            _departmentRepo = departmentRepo;
        }

        //Departments Actions

        [HttpGet("GetAll")]
        public IActionResult GetAllDepartments()
        {
            var departments = _departmentRepo.GetAll();

            if (departments is null) return NotFound("No departments were found");
            return Ok(departments);
        }

        [HttpGet("GetById")]
        public IActionResult GetDepartmentById(string id)
        {
            if (id is null) return BadRequest($"Department with id of {id} does not exist");

            return Ok(_departmentRepo.GetById(id));
        }

        [HttpGet("GetByName")]
        public IActionResult GetDepartmentByName(string name)
        {
            if (name is null) return BadRequest("Please enter a valid Department name");

            var department = _departmentRepo.GetByName(name);

            if (department is null) return NotFound($"Department with name {name} does not exist");

            return Ok(department);
        }

        [HttpPost("Add")]
        [Authorize(Roles = "1")]
        public IActionResult AddDepartment(CreateDepartmentRequest createDepartmentRequest)
        {
            if (createDepartmentRequest is null) return BadRequest("Please enter the required fields");

            Department newDepartment = new Department() 
            {
                Name = createDepartmentRequest.Name,
                Concentrations = createDepartmentRequest.Concentrations,
            };

            try
            {
                _departmentRepo.Create(newDepartment);
                return Ok($"Department {newDepartment.Name} was successfully created");
            }
            catch (Exception)
            {
                return BadRequest("Something went wrong. Please try again later.");
            }


        }

        [HttpPut("Edit")]
        [Authorize(Roles = "1")]
        public IActionResult EditDepartment(Department department)
        {
            var foundDepartment = _departmentRepo.GetById(department.Id);

            if (foundDepartment is null) return NotFound($"Existing department {department.Name} was not found; Please create a new one instead");

            try
            {
                _departmentRepo.Edit(department);
                return Ok($"Department {department.Name} Updated.");
            }
            catch (Exception)
            {

                return BadRequest($"Something went wrong. Please try again later");
            }
        }



        [HttpDelete("Delete")]
        [Authorize(Roles = "1")]
        public IActionResult DeleteDepartment(string id)
        {
            if (id is null) return BadRequest($"Department with ID {id} does not exist.");

            try
            {
                _departmentRepo.Delete(id);
                return Ok($"Department with ID {id} was deleted successfully");
            }
            catch (Exception)
            {
                return BadRequest("Something went wrong. Try again later.");
            }
        }



        //Concentrations Actions

        [HttpGet("Concentrations/GetAll")]
        public IActionResult GetAllConcentrations() 
        {
            var departments = _departmentRepo.GetAll();
            var concentrations = departments.SelectMany(d => d.Concentrations).ToList();

            return Ok(concentrations);
        }

        [HttpGet("Concentrations/GetByName")]
        public IActionResult GetConcentrationByName(string name)
        {
            var departments = _departmentRepo.GetAll();
            var foundConcentration = departments.SelectMany(d => d.Concentrations ?? Enumerable.Empty<Concentration>()).FirstOrDefault(c => c.Name == name);

            if (foundConcentration is null) return NotFound();

            return Ok(foundConcentration);
        }

    }
}
