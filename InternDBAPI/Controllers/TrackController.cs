﻿using InternDBAPI.Interfaces;
using InternDBAPI.Models;
using InternDBAPI.Models.RequestModel;
using InternDBAPI.Repos;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace InternDBAPI.Controllers
{
    [ApiController]
    [Route("[controller]")]
    [Authorize]

    public class TrackController : Controller
    {
        private readonly ITrackRepo _trackRepo;

        public TrackController(ITrackRepo trackRepo)
        {
            _trackRepo = trackRepo;
        }


        [HttpGet("GetAll")]
        [Authorize]
        public IActionResult GetAllTracks()
        {
            List<Track> tracks = _trackRepo.GetAllTracks();
            return Ok(tracks);
        }

        [HttpPost]
        [Authorize]
        public IActionResult CreateTrack(Track newTrack)
        {
            if (newTrack is null)
            {
                return BadRequest("Not a valid Track");
            }

            try
            {
                _trackRepo.CreateTrack(newTrack);
                return Ok(_trackRepo.GetAllTracks());
            }
            catch (Exception)
            {
                return BadRequest("Not a valid Track");
            }

        }

        [HttpDelete("Delete/{id}")]
        [Authorize]
        public IActionResult DeleteTrack([FromRoute] string id)
        {
            if (id is null)
            {
                return BadRequest("Not a valid Id");
            }

            _trackRepo.DeleteTrack(id);
            return Ok(_trackRepo.GetAllTracks());

        }



        [HttpPut("EditTracks")]
        [Authorize]
        public IActionResult EditTrack(List<Track> tracks)
        {
            if (tracks is null)
            {
                return BadRequest("Not a valid Track");
            }

            try
            {
                
                var tracksList = _trackRepo.UpdateAllTracks(tracks);
                return Ok(tracksList);
            }
            catch (Exception)
            {
                return BadRequest("An error occured while updating the track");
            }
        }
    }
}
