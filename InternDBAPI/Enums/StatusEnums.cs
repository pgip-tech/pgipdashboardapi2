﻿namespace InternDBAPI.Enums
{
    public enum StatusEnums
    {
        Intern,
        Gita,
        Dropped,
        Archived
    }
}
