﻿namespace InternDBAPI.Enums
{
    public enum DepartmentsEnum
    {
        AnalyticsAndBusiness,
        CyberSecurity,
        Database,
        ITSupport,
        ITSystemsNetwork,
        SoftwareAndWebDevelopment
    }
}
