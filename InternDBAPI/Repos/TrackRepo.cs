﻿using InternDBAPI.Interfaces;
using InternDBAPI.Models;
using Microsoft.VisualBasic;
using MongoDB.Driver;

namespace InternDBAPI.Repos
{
    public class TrackRepo : ITrackRepo
    {

        private readonly IInternDBSettings _internDBSettings;
        private readonly IMongoCollection<Track> _tracks;

        public TrackRepo(IInternDBSettings internDBSettings, IMongoClient mongoClient)
        {
            _internDBSettings = internDBSettings;
            var database = mongoClient.GetDatabase(_internDBSettings.DatabaseName);
            _tracks = database.GetCollection<Track>(_internDBSettings.TracksCollection);
        }

        public List<Track> GetAllTracks()
        {
            return _tracks.Find((t) => true).ToList();
        }

        public Track GetTrackById(string id)
        {
            return _tracks.Find((t) => t.Id == id).FirstOrDefault();
        }

        public Track GetTrackByName(string trackName)
        {
            return _tracks.Find((t) => t.TrackName == trackName).FirstOrDefault();
        }

        public void UpdateTrack(string id, Track track)
        {
            var foundTrack = _tracks.Find((i) => i.Id == track.Id).FirstOrDefault();

            if (foundTrack is not null)
            {
                foundTrack.TrackName = track.TrackName;
                foundTrack.StartDate = track.StartDate;
                foundTrack.EndDate = track.EndDate;
            }

            _tracks.ReplaceOne(t => t.Id == id, track);
        }

        public List<Track> UpdateAllTracks(List<Track> tracks)
        {
            foreach (var track in tracks) 
            {
                var filter = Builders<Track>.Filter.Eq(t => t.Id, track.Id);

                var update = Builders<Track>.Update
                    .Set(t => t.StartDate, track.StartDate)
                    .Set(t => t.EndDate, track.EndDate)
                    .Set(t => t.TrackName, track.TrackName);

                var result = _tracks.UpdateOne(filter, update);
            }

            return GetAllTracks();
        }


        public void CreateTrack(Track track)
        {
            _tracks.InsertOne(track);
        }

        public void DeleteTrack(string id)
        {
            _tracks.DeleteOne(t => t.Id == id);
        }
    }
}
