﻿using InternDBAPI.Enums;
using InternDBAPI.Interfaces;
using InternDBAPI.Models;
using InternDBAPI.Models.RequestModel;
using MongoDB.Driver;
using System.Security.AccessControl;

namespace InternDBAPI.Repos
{
    public class InternRepo : IInternRepo
    {
        private readonly IInternDBSettings _internDBSettings;
        private readonly IMongoCollection<Intern> _interns;


        public InternRepo(IInternDBSettings internDBSettings, IMongoClient mongoClient)
        {
            _internDBSettings = internDBSettings;
            var database = mongoClient.GetDatabase(_internDBSettings.DatabaseName);
            _interns = database.GetCollection<Intern>(_internDBSettings.InternsCollection);
        }


        public Intern CreateIntern(Intern intern)
        {
            _interns.InsertOne(intern);
            return intern;
        }

        public List<Intern> GetAll()
        {
            return _interns.Find(intern => true).ToList();
        }

        public Intern GetById(string id)
        {
            return _interns.Find(user => user.Id == id).FirstOrDefault();
        }

        public List<Intern> GetByDepartment(string department)
        {
            return _interns.Find(intern => intern.Department.ToString() == department).ToList();
        }

        public Intern GetByPgipEmail(string email)
        {
            return _interns.Find(intern => intern.PGIPEmail == email).FirstOrDefault();
        }

        public void Update(string id, Intern intern)
        {
            _interns.ReplaceOne(intern => intern.Id == id, intern);
        }
    }
}
