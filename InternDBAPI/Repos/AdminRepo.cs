﻿using InternDBAPI.Interfaces;
using InternDBAPI.Models;
using InternDBAPI.Services;
using MongoDB.Driver;

namespace InternDBAPI.Repos
{
    public class AdminRepo : IAdminRepo
    {
        private readonly IInternDBSettings _internDBSettings;
        private readonly IMongoCollection<Admin> _admins;

        public AdminRepo(IInternDBSettings internDBSettings, IMongoClient mongoClient)
        {
            _internDBSettings = internDBSettings;
            var database = mongoClient.GetDatabase(_internDBSettings.DatabaseName);
            _admins = database.GetCollection<Admin>(_internDBSettings.AdminsCollection);
        }

        public List<Admin> GetAll()
        {
            return _admins.Find(admin => true).ToList();
        }

        public Admin GetById(string id)
        {
            return _admins.Find(admin => admin.Id == id).FirstOrDefault();
        }

        public Admin GetByEmail(string email)
        { 
            return _admins.Find(admin => admin.Email == email).FirstOrDefault();
        }

        public void Delete(string id)
        {
            _admins.DeleteOne(admin => admin.Id == id);
        }

        public Admin Create(Admin admin)
        {
            _admins.InsertOne(admin);
            return admin;
        }
    }
}
