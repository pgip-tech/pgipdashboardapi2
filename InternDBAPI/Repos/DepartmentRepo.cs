﻿using InternDBAPI.Interfaces;
using InternDBAPI.Models;
using Microsoft.AspNetCore.Http.HttpResults;
using MongoDB.Driver;

namespace InternDBAPI.Repos
{
    public class DepartmentRepo : IDepartmentRepo
    {
        private readonly IInternDBSettings _internDBSettings;
        private readonly IMongoCollection<Department> _departments;

        public DepartmentRepo(IInternDBSettings internDBSettings, IMongoClient mongoClient)
        {
            _internDBSettings = internDBSettings;
            var database = mongoClient.GetDatabase(_internDBSettings.DatabaseName);
            _departments = database.GetCollection<Department>(_internDBSettings.DepartmentsCollection);
        }

        public List<Department> GetAll()
        {
           return _departments.Find((d) => true).ToList();
        }

        public Department GetById(string id)
        {
            return _departments.Find(d => d.Id == id).SingleOrDefault();
        }

        public Department GetByName(string name)
        {
            return _departments.Find(d => d.Name == name).SingleOrDefault();
        }

        public void Create(Department department)
        {
            _departments.InsertOne(department);
        }

        public void Edit(Department newDepartment)
        {
            var department = GetById(newDepartment.Id);
            department.Name = newDepartment.Name;
            department.Concentrations = newDepartment.Concentrations;

            _departments.ReplaceOne((d) => d.Id == department.Id, department);
        }

        public void Delete(string id)
        {
            _departments.DeleteOne(d => d.Id == id);
        }
    }
}
